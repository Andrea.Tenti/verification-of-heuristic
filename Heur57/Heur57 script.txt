% Let q be a Mersenne prime and let U={0,...,l2-1} be a subset
% of Z/qZ.
% We pick lim random elements from Z/qZ. For every element r
% chosen in this way, we define V={r,...,l+r-1} in Z/qZ.
% For every nonzero element i in Z/qZ, we compute the image of U
% under multiplication times i and record the size of the
% intersection of the image with V.
% We measure the difference between the measured intersection
% and Heuristic 5.7.
% We also report the average proportion of elements from Z/qZ
% that yielded an intersection of size at most 1.





% ------------------------------
% Inputs
% ------------------------------
q=8191;
l=floor(sqrt(q))-10;					%size of V
l2=floor(q/l);							%size of U
lim=100;

% ------------------------------
%Initialization
% ------------------------------
m=[];
em=l*l2/q;								%expected number of solutions according to the heuristic.
g_s=[];
L=zeros(lim,q-1);
R=[];

% ------------------------------
% Main cycle
% ------------------------------
for k=1:lim
	r=randi(q);
	R=[R,r];							%record of the random elements picked
	for i=1:q-1
		S=0;
		for j=0:l2-1					%counting the number of elements from U landing in V
			if mod(i*j-r,q) <l
				S=S+1;
			end
		end
		L(k,i)=S;
		i;
	end

	m=[m,sum(L(k,:))/length(L(k,:))];	%size of the intersection
	cont=0;								%counter for good_system
	for i=1:length(L(k,:))				%counting the number of systems with at most one solution
		if L(k,i)<2
			cont=cont+1;
		end
	end
	g_s=[g_s,cont/length(L(k,:))];		%proportion of systems with at most one solution.
end

% ------------------------------
% Output
% ------------------------------
V=var(m);
diff=em-mean(m);						%difference between the value provided by the heuristic and the measured one
M=mean(g_s);